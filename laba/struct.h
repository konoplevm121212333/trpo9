#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <array>
#include <algorithm>

using namespace std;

std::fstream in("C:\\Users\\Maxim\\git\\file.txt");

struct info
{
	float price;
	int amount;
	string type;
	string exact;
};

info insertinfo()
{
	info data;
	in >> data.price;
	in >> data.amount;
	in >> data.type;
	in >> data.exact;
	return data;
};

void outinfo(info data) {
	cout << data.price << ' ';
	cout << data.amount << ' ';
	cout << data.type << ' ';
	cout << data.exact << ' ';
	cout << endl;
};

void showtype(info* data, int n)
{
	for (int i = 0; i < n; i++)
	{
		if (data[i].type == "Промтовары" || data[i].type == "промтовары") {
			outinfo(data[i]);
		}
	}
};

void compareprices(info* data, int n) {
	for (int i = 0; i < n; i++)
	{
		if (data[i].price > 100)
			outinfo(data[i]);
	}
};

void swap(int* xp, int* yp)
{
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void merge(float array[], int const left, int const mid,
    int const right)
{
    auto const subArrayOne = mid - left + 1;
    auto const subArrayTwo = right - mid;

    auto* leftArray = new float[subArrayOne],
        * rightArray = new float[subArrayTwo];

    for (auto i = 0; i < subArrayOne; i++)
        leftArray[i] = array[left + i];
    for (auto j = 0; j < subArrayTwo; j++)
        rightArray[j] = array[mid + 1 + j];

    auto indexOfSubArrayOne
        = 0, 
        indexOfSubArrayTwo
        = 0; 
    auto indexOfMergedArray
        = left; 

    while (indexOfSubArrayOne < subArrayOne
        && indexOfSubArrayTwo < subArrayTwo) {
        if (leftArray[indexOfSubArrayOne]
            >= rightArray[indexOfSubArrayTwo]) {
            array[indexOfMergedArray]
                = leftArray[indexOfSubArrayOne];
            indexOfSubArrayOne++;
        }
        else {
            array[indexOfMergedArray]
                = rightArray[indexOfSubArrayTwo];
            indexOfSubArrayTwo++;
        }
        indexOfMergedArray++;
    }

    while (indexOfSubArrayOne < subArrayOne) {
        array[indexOfMergedArray]
            = leftArray[indexOfSubArrayOne];
        indexOfSubArrayOne++;
        indexOfMergedArray++;
    }

    while (indexOfSubArrayTwo < subArrayTwo) {
        array[indexOfMergedArray]
            = rightArray[indexOfSubArrayTwo];
        indexOfSubArrayTwo++;
        indexOfMergedArray++;
    }
    delete[] leftArray;
    delete[] rightArray;
}

void mergeSort(float array[], int const begin, int const end)
{
    if (begin >= end)
        return;

    auto mid = begin + (end - begin) / 2;
    mergeSort(array, begin, mid);
    mergeSort(array, mid + 1, end);
    merge(array, begin, mid, end);
}