#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <fstream>


using namespace std;

#include "struct.h"
#include "constants.h"

int main()
{
    cout << "Вариант 10. Каталог товаров. Коноплёв М. Д." << endl;
    ifstream inp("C:\\Users\\Maxim\\git\\file.txt");
    char buf[256];
    int count = 0;
    while (inp.getline(buf, 256))
        count++;
    const int n = count;
    info* data = new info[n];
    for (int i = 0; i < n; i++)
    {
        data[i] = insertinfo();
    }
    for (int i = 0; i < n; i++)
    {
        outinfo(data[i]);
    }
    
    
    
    cout << endl << "Все товары с ценой выше 100:" << endl;
    compareprices(data, n);
    
    
    
    cout << endl << "Все товары категории промтовары:" << endl;
    showtype(data, n);


     
    int* dataamount = new int[n];
    for (int i = 0; i < n; i++)
    {
        dataamount[i] = data[i].amount;
    }
    int i, j, min_idx;
    for (i = 0; i < n - 1; i++)
    {
        min_idx = i;
        for (j = i + 1; j < n; j++)
            if (dataamount[j] > dataamount[min_idx])
                min_idx = j;
        if (min_idx != i)
            swap(&dataamount[min_idx], &dataamount[i]);
    }
    cout << endl << "Товары отсортированы по убыванию количества товара (сортировка выбором):" << endl;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (data[j].amount == dataamount[i])
            {
                outinfo(data[j]);
            }
        }
    }
   
    
    
    float* dataprice = new float[n];
    for (int i = 0; i < n; i++)
    {
        dataprice[i] = data[i].price;
    }
    mergeSort(dataprice, 0, n-1);
    cout << endl << "Товары отсортированы по убыванию цены товара (сортировка слиянием):" << endl;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (data[j].price == dataprice[i])
            {
                outinfo(data[j]);
            }
        }
    }
}

/*("C:\\Users\\Maxim\\git\\file.txt")*/
